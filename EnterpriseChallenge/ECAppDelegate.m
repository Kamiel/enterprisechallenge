//
//  ECAppDelegate.m
//  EnterpriseChallenge
//
//  Created by Yuriy Pitomets on 5/21/15.
//  Copyright (c) 2015 Yuriy Pitomets. All rights reserved.
//

#import "ECAppDelegate.h"

@interface ECAppDelegate ()

@end

@implementation ECAppDelegate


- (BOOL)            application:(UIApplication *)application
  didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UITabBar.appearance.tintColor = UIColor.whiteColor;
    NSDictionary* whiteAttribute = @{NSForegroundColorAttributeName : UIColor.whiteColor};
    [UITabBarItem.appearance setTitleTextAttributes:whiteAttribute
                                           forState:UIControlStateSelected];
    [UITabBarItem.appearance setTitleTextAttributes:whiteAttribute
                                           forState:UIControlStateNormal];
    UITabBar.appearance.shadowImage = nil;
    UIApplication.sharedApplication.statusBarStyle = UIStatusBarStyleLightContent;
    UINavigationBar.appearance.barStyle = UIBarStyleBlackOpaque;
    UINavigationBar.appearance.backgroundColor = UIColor.blackColor;
    UITabBar.appearance.backgroundImage = [UIImage imageNamed:@"tab_bar_bg"];
    UITabBar.appearance.backgroundColor = UIColor.blackColor;
    self.window.backgroundColor = UIColor.whiteColor;

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
