//
//  ECAppDelegate.h
//  EnterpriseChallenge
//
//  Created by Yuriy Pitomets on 5/21/15.
//  Copyright (c) 2015 Yuriy Pitomets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ECAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

