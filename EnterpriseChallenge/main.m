//
//  main.m
//  EnterpriseChallenge
//
//  Created by Yuriy Pitomets on 5/21/15.
//  Copyright (c) 2015 Yuriy Pitomets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ECAppDelegate class]));
    }
}
